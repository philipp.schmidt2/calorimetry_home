from functions import m_json
from functions import m_pck
import pprint


path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata_dict = m_json.get_metadata_from_setup(path)


folder_path = "/home/pi/calorimetry_home/datasheets"
metadata = m_json.add_temperature_sensor_serials(folder_path, metadata_dict)


data = m_pck.get_meta_data_calorimetry(metadata)

data_path = "/home/pi/calorimetry_home/data/versuch2"

m_pck.logging_calorimetry(data,metadata,data_path,folder_path) 



m_json.archiv_json(folder_path, path, data_path)